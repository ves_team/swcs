package start;

import java.awt.Desktop;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import logic.ConfigFile;
import logic.Excel;
import logic.GUI_ProgressBar;
import logic.GUI_Sequence;
import logic.GUI_timeScale;
import logic.Import;
import logic.Kanban;
import logic.Manifest;
import logic.SystemSettings;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.xml.sax.SAXException;

public class Scws {
	
	// variables

	public static String projectName = "";
	public static String galleryTitle = "";
	public static String gallerySubtitle = "";
	public static String pathToTemplateFcwFile = "";
	
	static public Boolean deleteDirectory( String emplacement )
	{
	  File path = new File( emplacement );
	  if( path.exists() )
	  {
	    File[] files = path.listFiles();
	    for( int i = 0 ; i < files.length ; i++ )
	    {
	      if( files[ i ].isDirectory() )
	      {
	        deleteDirectory( path+"\\"+files[ i ] );
	        System.out.println("change  directory");
	      }
	      files[ i ].delete();
	      System.out.println("----del----");
	    }
	  }
	  return true;
	}
	
	// start application
	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException, XPathExpressionException, TransformerException {
		System.out.println("V1.8");
		// 1) LOAD PARAMETERS FROM CONFIG FILE
		System.out.println("STEP 1 : LOAD PARAMETERS FROM CONFIG FILE");
		ConfigFile config = new ConfigFile(); // load parameters for Excel
		SystemSettings systemSettings = new SystemSettings();
		 projectName = config.getProjectName();
		 galleryTitle = config.getTitles();
		 gallerySubtitle = config.getSubitles();
		 pathToTemplateFcwFile = config.getTemplatePath();
		
		systemSettings.getOSInformations();
		
		if (deleteDirectory("C:\\Users\\Public\\Documents\\SMART Technologies\\Gallery\\Added Content\\inbox"))
			System.out.println("----------------del-inbox----------------");
		if (deleteDirectory("C:\\Users\\Public\\Documents\\SMART Technologies\\Gallery\\Added Content"))
			System.out.println("----------------del-----------------");
		
		// 2) USE INFORMATION FROM EXCEL
		System.out.println("STEP 2 : USE INFORMATION FROM EXCEL");
		final Excel excel = new Excel();
		File excelFile;
		String[][] arrExcel = null;

		// choose excel file
		excelFile = excel.chooseFile(config.getStandardPathForFileChooser());
		GUI_ProgressBar gui_ProgressBar = new GUI_ProgressBar();
		gui_ProgressBar.setProgressCount( " read data records from excel file ");

		// read excel informations
		XSSFSheet sheet = excel.initializeExcel(excelFile, Integer.parseInt(config.getSheet()));
		arrExcel = excel.readXLSX(sheet,
				Integer.parseInt(config.getRowFirst()), Integer.parseInt(config.getRowLast()),
				Integer.parseInt(config.getColumnFirst()), Integer.parseInt(config.getColumnLast()));
		

		
				
		// 3) PREPARE MANIFESTS
		System.out.println("STEP 3 : PROVIDE MANIFESTS");
		// show progress bar
		gui_ProgressBar.setProgressCount( " extract information ");
		Manifest manifest = new Manifest();
		
		// provide user manifest
		manifest.newUserManifest(projectName, galleryTitle, gallerySubtitle);
		manifest.parseUserManifest(); // load user manifest

		
		
		// 4) CREATE KANBANS
		System.out.println("STEP 4 : CREATE KANBANS");
		Kanban kanban = new Kanban();
		GUI_Sequence sequence = null;
		GUI_timeScale timeScale =null;
		
		// create kanban folders 
		kanban.createSubFolder();
		
		// create kanban metadata
		kanban.createMetadata("swcs"); // set kanban title in gallery as sequence number

		for (int i = 0; i < arrExcel.length; i++) {
			gui_ProgressBar.setProgressCount( " step : "+(i+1)+" / "+arrExcel.length+" adding to the sequence");
			// create kanban GUI
			if (i < arrExcel.length ) {
				sequence = new GUI_Sequence(arrExcel[i], false);
			} 
			if (i == arrExcel.length - 1) {
				sequence = new GUI_Sequence(arrExcel[i], true); // total, last record
			}
			
			// create kanban picture
			BufferedImage imgSnapshot0 = kanban.getSnapShot(sequence.getNumber());
			BufferedImage imgSnapshot1 = kanban.getSnapShot(sequence.getTitle());
			BufferedImage imgSnapshot2 = kanban.getSnapShot(sequence.getManualTime());
			BufferedImage imgSnapshot3 = kanban.getSnapShot(sequence.getMachineTime());
			BufferedImage imgSnapshot4 = kanban.getSnapShot(sequence.getMoveTime());
			ImageIO.write(imgSnapshot0, "png", new File(kanban.getToKanban().getPath()+"/images/kanban_0_" + i + ".png"));
			ImageIO.write(imgSnapshot1, "png", new File(kanban.getToKanban().getPath()+"/images/kanban_1_" + i + ".png"));
			ImageIO.write(imgSnapshot2, "png", new File(kanban.getToKanban().getPath()+"/images/kanban_2_" + i + ".png"));
			ImageIO.write(imgSnapshot3, "png", new File(kanban.getToKanban().getPath()+"/images/kanban_3_" + i + ".png"));
			ImageIO.write(imgSnapshot4, "png", new File(kanban.getToKanban().getPath()+"/images/kanban_4_" + i + ".png"));
			if (i == arrExcel.length - 1) {
				
				System.out.println("  created kanban picture");
			}
			
			// create kanban preview
			if (i == arrExcel.length - 1) {
				BufferedImage imgSnapshot = kanban.getSnapShot(sequence.getPreview());
				ImageIO.write(imgSnapshot, "png", new File(kanban.getToKanban().getPath()+"/preview.png"));
				imgSnapshot.flush();
				System.out.println("  created kanban preview");
			}
		}
		
		
		// create kanban SVG 
		kanban.createSVG(arrExcel);
		
		System.out.println(">>>>>>>>>>>>>>>>>>>>> unity of time : "+kanban.getTimeUnity());
		timeScale = new GUI_timeScale(kanban.getTimeUnity());
		BufferedImage imgSnapshotTime0 = kanban.getSnapShot(timeScale.getTimeScale0());
		ImageIO.write(imgSnapshotTime0, "png", new File(kanban.getToKanban().getPath()+"/images/timeScale0.png"));
		BufferedImage imgSnapshotTime1 = kanban.getSnapShot(timeScale.getTimeScale1());
		ImageIO.write(imgSnapshotTime1, "png", new File(kanban.getToKanban().getPath()+"/images/timeScale1.png"));
		BufferedImage imgSnapshotTime2 = kanban.getSnapShot(timeScale.getTimeScale2());
		ImageIO.write(imgSnapshotTime2, "png", new File(kanban.getToKanban().getPath()+"/images/timeScale2.png"));
		BufferedImage imgSnapshotTime3 = kanban.getSnapShot(timeScale.getTimeScale3());
		ImageIO.write(imgSnapshotTime3, "png", new File(kanban.getToKanban().getPath()+"/images/timeScale3.png"));
		BufferedImage imgSnapshotTime4 = kanban.getSnapShot(timeScale.getTimeScale4());
		ImageIO.write(imgSnapshotTime4, "png", new File(kanban.getToKanban().getPath()+"/images/timeScale4.png"));
		BufferedImage imgSnapshotTime5 = kanban.getSnapShot(timeScale.getTimeScale5());
		ImageIO.write(imgSnapshotTime5, "png", new File(kanban.getToKanban().getPath()+"/images/timeScale5.png"));
		BufferedImage imgSnapshotTime6 = kanban.getSnapShot(timeScale.getTimeScale6());
		ImageIO.write(imgSnapshotTime6, "png", new File(kanban.getToKanban().getPath()+"/images/timeScale6.png"));
		BufferedImage imgSnapshotTime7 = kanban.getSnapShot(timeScale.getTimeScale7());
		ImageIO.write(imgSnapshotTime7, "png", new File(kanban.getToKanban().getPath()+"/images/timeScale7.png"));
		BufferedImage imgSnapshotTime8 = kanban.getSnapShot(timeScale.getTimeScale8());
		ImageIO.write(imgSnapshotTime8, "png", new File(kanban.getToKanban().getPath()+"/images/timeScale8.png"));
		BufferedImage imgSnapshotTime9 = kanban.getSnapShot(timeScale.getTimeScale9());
		ImageIO.write(imgSnapshotTime9, "png", new File(kanban.getToKanban().getPath()+"/images/timeScale9.png"));
		BufferedImage imgSnapshotTime10 = kanban.getSnapShot(timeScale.getTimeScale10());
		ImageIO.write(imgSnapshotTime10, "png", new File(kanban.getToKanban().getPath()+"/images/timeScale10.png"));
		BufferedImage imgSnapshotTime11 = kanban.getSnapShot(timeScale.getTimeScale11());
		ImageIO.write(imgSnapshotTime11, "png", new File(kanban.getToKanban().getPath()+"/images/timeScale11.png"));
		BufferedImage imgSnapshotTime12 = kanban.getSnapShot(timeScale.getTimeScale12());
		ImageIO.write(imgSnapshotTime12, "png", new File(kanban.getToKanban().getPath()+"/images/timeScale12.png"));
		BufferedImage imgSnapshotTime13 = kanban.getSnapShot(timeScale.getTimeScale13());
		ImageIO.write(imgSnapshotTime13, "png", new File(kanban.getToKanban().getPath()+"/images/timeScale13.png"));
		BufferedImage imgSnapshotTime14 = kanban.getSnapShot(timeScale.getTimeScale14());
		ImageIO.write(imgSnapshotTime14, "png", new File(kanban.getToKanban().getPath()+"/images/timeScale14.png"));
		BufferedImage imgSnapshotTime15 = kanban.getSnapShot(timeScale.getTimeScale15());
		ImageIO.write(imgSnapshotTime15, "png", new File(kanban.getToKanban().getPath()+"/images/timeScale15.png"));
		BufferedImage imgSnapshotTime16 = kanban.getSnapShot(timeScale.getTimeScale16());
		ImageIO.write(imgSnapshotTime16, "png", new File(kanban.getToKanban().getPath()+"/images/timeScale16.png"));
		
		// create kanban .galleryitem (save it in folder "new items")
		String inputGalleryitemFilesFolders = kanban.getToKanban().getPath();
		String outputGalleryitem = kanban.getNewItems().getPath() + File.separator + "full sequence.galleryitem";
		kanban.compressZip(outputGalleryitem, inputGalleryitemFilesFolders);
		
		// hide progress bar information
		gui_ProgressBar.setFrameVisibility(false);
		
		// 5) IMPORT TO GALLERY
		System.out.println("STEP 5 : IMPORT TO GALLERY");
		Import cImport = new Import();
		
		// import kanbans to gallery title
		cImport.importKanbansFromNewItems();
		
		JFrame PopUpCreated = new JFrame("JOptionPane showMessageDialog");
		JOptionPane.showMessageDialog(PopUpCreated, "Sequence created !");
		
		
		// 6) START SMART MEETING PRO
		System.out.println("STEP 6 : OPEN SMART MEETING PRO");
		
		if (deleteDirectory("C:\\Users\\Public\\Documents\\SMART Technologies\\Gallery\\Added Content\\to kanban"))
			System.out.println("----del to kanban----");
		if (deleteDirectory("C:\\Users\\Public\\Documents\\SMART Technologies\\Gallery\\Added Content\\to kanban\\images"))
			System.out.println("----del to kanban images----");
		
		// start template file (*.fcw)
		File template = new File (pathToTemplateFcwFile);
		
		if (pathToTemplateFcwFile.equals("UNKNOWN") || !template.exists()){
			// variables
			File choosedFile = null;
			
			JFrame PopUpWarningTemplate= new JFrame("JOptionPane showMessageDialog");
			JOptionPane.showMessageDialog(PopUpWarningTemplate, "Template not found ! \n the template have been removed or deleted frome this palce: "
					+pathToTemplateFcwFile+"\n Please selected a new template","missing template", JOptionPane.ERROR_MESSAGE);
			
			// JFileChooser dialog box
			JFileChooser fc = new JFileChooser(System.getProperty("user.home") + "/Desktop");
		    FileNameExtensionFilter filter = new FileNameExtensionFilter("FCW files", "fcw"); // show only fcw files to choose
		    fc.setFileFilter(filter);
		    int rueckgabeWert = fc.showOpenDialog(null); // open dialog box and look for return statement
	    
		    // if user choosed a valid file? 
		    if(rueckgabeWert == JFileChooser.APPROVE_OPTION) {
		    	File file = fc.getSelectedFile();
		        choosedFile = file.getAbsoluteFile();
				System.out.println("  choose fcw file: "+file.getName());
		    } else {
		    	System.out.println("  Exit: no fcw file choosed");
		    	System.exit(0);
		    }
			
			pathToTemplateFcwFile = choosedFile.getAbsolutePath();
			template = new File (pathToTemplateFcwFile);
		}		
		Desktop.getDesktop().open(new File(pathToTemplateFcwFile));			
		System.exit(0);
	}
}