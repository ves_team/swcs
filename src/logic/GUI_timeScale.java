package logic;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import java.awt.Color;
import java.awt.Font;

@SuppressWarnings("serial")
public class GUI_timeScale extends JPanel{
	private JPanel contentPane,contentPane0,contentPane1,contentPane2,contentPane3,contentPane4,contentPane5,contentPane6,contentPane7,contentPane8,
										contentPane9,contentPane10,contentPane11,contentPane12,contentPane13,contentPane14,contentPane15,contentPane16;
	
	public GUI_timeScale(Double timeUnity){

	System.out.println(">>>>>>>>>>>>>>>>>>>>> unity of time : "+timeUnity);

	contentPane = new JPanel();
	contentPane.setBackground(Color.LIGHT_GRAY);
	contentPane.setOpaque(true); // do not show background
	contentPane.setBounds(0, 0, 1020, 30); // x-Pos, y-Pos, width, height
	

	contentPane0 = new JPanel();
	contentPane0.setOpaque(false); // do not show background
	contentPane0.setBounds(0, 0, 60, 30); // x-Pos, y-Pos, width, height
	
	contentPane1 = new JPanel();
	contentPane1.setOpaque(false); // do not show background
	contentPane1.setBounds(60, 0, 60, 30); // x-Pos, y-Pos, width, height

	contentPane2 = new JPanel();
	contentPane2.setOpaque(false); // do not show background
	contentPane2.setBounds(120, 0, 60, 30); // x-Pos, y-Pos, width, height
	
	contentPane3 = new JPanel();
	contentPane3.setOpaque(false); // do not show background
	contentPane3.setBounds(180, 0, 60, 30); // x-Pos, y-Pos, width, height
	
	contentPane4 = new JPanel();
	contentPane4.setOpaque(false); // do not show background
	contentPane4.setBounds(240, 0, 60, 30); // x-Pos, y-Pos, width, height
	
	contentPane5 = new JPanel();
	contentPane5.setOpaque(false); // do not show background
	contentPane5.setBounds(300, 0, 60, 30); // x-Pos, y-Pos, width, height
	
	contentPane6 = new JPanel();
	contentPane6.setOpaque(false); // do not show background
	contentPane6.setBounds(360, 0, 60, 30); // x-Pos, y-Pos, width, height
	
	contentPane7 = new JPanel();
	contentPane7.setOpaque(false); // do not show background
	contentPane7.setBounds(420, 0, 60, 30); // x-Pos, y-Pos, width, height
	
	contentPane8 = new JPanel();
	contentPane8.setOpaque(false); // do not show background
	contentPane8.setBounds(480, 0, 60, 30); // x-Pos, y-Pos, width, height
	
	contentPane9 = new JPanel();
	contentPane9.setOpaque(false); // do not show background
	contentPane9.setBounds(540, 0, 60, 30); // x-Pos, y-Pos, width, height

	contentPane10 = new JPanel();
	contentPane10.setOpaque(false); // do not show background
	contentPane10.setBounds(600, 0, 60, 30); // x-Pos, y-Pos, width, height

	contentPane11 = new JPanel();
	contentPane11.setOpaque(false); // do not show background
	contentPane11.setBounds(660, 0, 60, 30); // x-Pos, y-Pos, width, height

	contentPane12 = new JPanel();
	contentPane12.setOpaque(false); // do not show background
	contentPane12.setBounds(720, 0, 60, 30); // x-Pos, y-Pos, width, height

	contentPane13 = new JPanel();
	contentPane13.setOpaque(false); // do not show background
	contentPane13.setBounds(780, 0, 60, 30); // x-Pos, y-Pos, width, height

	contentPane14 = new JPanel();
	contentPane14.setOpaque(false); // do not show background
	contentPane14.setBounds(840, 0, 60, 30); // x-Pos, y-Pos, width, height

	contentPane15 = new JPanel();
	contentPane15.setOpaque(false); // do not show background
	contentPane15.setBounds(900, 0, 60, 30); // x-Pos, y-Pos, width, height

	contentPane16 = new JPanel();
	contentPane16.setOpaque(false); // do not show background
	contentPane16.setBounds(960, 0, 60, 30); // x-Pos, y-Pos, width, height

	
	
	// Number
	JLabel Number0 = new JLabel(String.valueOf(0*timeUnity));
	Number0.setFont(new Font("Tahoma", Font.BOLD, 16));
	Number0.setHorizontalAlignment(SwingConstants.CENTER);
	Number0.setBounds(0, 0, 60, 30); // x-Pos, y-Pos, width, height
	
	JLabel Number1 = new JLabel(String.valueOf(10*timeUnity));
	Number1.setFont(new Font("Tahoma", Font.BOLD, 16));
	Number1.setHorizontalAlignment(SwingConstants.CENTER);
	Number1.setBounds(0, 0, 60, 30); // x-Pos, y-Pos, width, height
	
	JLabel Number2 = new JLabel(String.valueOf(20*timeUnity));
	Number2.setFont(new Font("Tahoma", Font.BOLD, 16));
	Number2.setOpaque(false); // do not show background
	Number2.setHorizontalAlignment(SwingConstants.CENTER);
	Number2.setBounds(0, 0, 60, 30); // x-Pos, y-Pos, width, height
	
	JLabel Number3 = new JLabel(String.valueOf(30*timeUnity));
	Number3.setFont(new Font("Tahoma", Font.BOLD, 16));
	Number3.setOpaque(false); // do not show background
	Number3.setHorizontalAlignment(SwingConstants.CENTER);
	Number3.setBounds(0, 0, 60, 30); // x-Pos, y-Pos, width, height
	
	JLabel Number4 = new JLabel(String.valueOf(40*timeUnity));
	Number4.setFont(new Font("Tahoma", Font.BOLD, 16));
	Number4.setOpaque(false); // do not show background
	Number4.setHorizontalAlignment(SwingConstants.CENTER);
	Number4.setBounds(0, 0, 60, 30); // x-Pos, y-Pos, width, height
	
	JLabel Number5 = new JLabel(String.valueOf(50*timeUnity));
	Number5.setFont(new Font("Tahoma", Font.BOLD, 16));
	Number5.setOpaque(false); // do not show background
	Number5.setHorizontalAlignment(SwingConstants.CENTER);
	Number5.setBounds(0, 0, 60, 30); // x-Pos, y-Pos, width, height
	
	JLabel Number6 = new JLabel(String.valueOf(60*timeUnity));
	Number6.setFont(new Font("Tahoma", Font.BOLD, 16));
	Number6.setOpaque(false); // do not show background
	Number6.setHorizontalAlignment(SwingConstants.CENTER);
	Number6.setBounds(0, 0, 60, 30); // x-Pos, y-Pos, width, height
	
	JLabel Number7 = new JLabel(String.valueOf(70*timeUnity));
	Number7.setFont(new Font("Tahoma", Font.BOLD, 16));
	Number7.setOpaque(false); // do not show background
	Number7.setHorizontalAlignment(SwingConstants.CENTER);
	Number7.setBounds(0, 0, 60, 30); // x-Pos, y-Pos, width, height
	
	JLabel Number8 = new JLabel(String.valueOf(80*timeUnity));
	Number8.setFont(new Font("Tahoma", Font.BOLD, 16));
	Number8.setOpaque(false); // do not show background
	Number8.setHorizontalAlignment(SwingConstants.CENTER);
	Number8.setBounds(0, 0, 60, 30); // x-Pos, y-Pos, width, height
	
	JLabel Number9 = new JLabel(String.valueOf(90*timeUnity));
	Number9.setFont(new Font("Tahoma", Font.BOLD, 16));
	Number9.setOpaque(false); // do not show background
	Number9.setHorizontalAlignment(SwingConstants.CENTER);
	Number9.setBounds(0, 0, 60, 30); // x-Pos, y-Pos, width, height

	JLabel Number10 = new JLabel(String.valueOf(100*timeUnity));
	Number10.setFont(new Font("Tahoma", Font.BOLD, 16));
	Number10.setOpaque(false); // do not show background
	Number10.setHorizontalAlignment(SwingConstants.CENTER);
	Number10.setBounds(0, 0, 60, 30); // x-Pos, y-Pos, width, height

	JLabel Number11 = new JLabel(String.valueOf(110*timeUnity));
	Number11.setFont(new Font("Tahoma", Font.BOLD, 16));
	Number11.setOpaque(false); // do not show background
	Number11.setHorizontalAlignment(SwingConstants.CENTER);
	Number11.setBounds(0, 0, 60, 30); // x-Pos, y-Pos, width, height

	JLabel Number12 = new JLabel(String.valueOf(120*timeUnity));
	Number12.setFont(new Font("Tahoma", Font.BOLD, 16));
	Number12.setOpaque(false); // do not show background
	Number12.setHorizontalAlignment(SwingConstants.CENTER);
	Number12.setBounds(0, 0, 60, 30); // x-Pos, y-Pos, width, height

	JLabel Number13 = new JLabel(String.valueOf(130*timeUnity));
	Number13.setFont(new Font("Tahoma", Font.BOLD, 16));
	Number13.setOpaque(false); // do not show background
	Number13.setHorizontalAlignment(SwingConstants.CENTER);
	Number13.setBounds(0, 0, 60, 30); // x-Pos, y-Pos, width, height

	JLabel Number14 = new JLabel(String.valueOf(140*timeUnity));
	Number14.setFont(new Font("Tahoma", Font.BOLD, 16));
	Number14.setOpaque(false); // do not show background
	Number14.setHorizontalAlignment(SwingConstants.CENTER);
	Number14.setBounds(0, 0, 60, 30); // x-Pos, y-Pos, width, height

	JLabel Number15 = new JLabel(String.valueOf(150*timeUnity));
	Number15.setFont(new Font("Tahoma", Font.BOLD, 16));
	Number15.setOpaque(false); // do not show background
	Number15.setHorizontalAlignment(SwingConstants.CENTER);
	Number15.setBounds(0, 0, 60, 30); // x-Pos, y-Pos, width, height

	JLabel Number16 = new JLabel(String.valueOf(160*timeUnity));
	Number16.setFont(new Font("Tahoma", Font.BOLD, 16));
	Number16.setOpaque(false); // do not show background
	Number16.setHorizontalAlignment(SwingConstants.CENTER);
	Number16.setBounds(0, 0, 60, 30); // x-Pos, y-Pos, width, height

	
	contentPane.setLayout(null);
	contentPane0.setLayout(null);
	contentPane1.setLayout(null);
	contentPane2.setLayout(null);
	contentPane3.setLayout(null);
	contentPane4.setLayout(null);
	contentPane5.setLayout(null);
	contentPane5.setLayout(null);
	contentPane7.setLayout(null);
	contentPane8.setLayout(null);
	contentPane9.setLayout(null);
	contentPane10.setLayout(null);
	contentPane11.setLayout(null);
	contentPane12.setLayout(null);
	contentPane13.setLayout(null);
	contentPane14.setLayout(null);
	contentPane15.setLayout(null);
	contentPane16.setLayout(null);
	contentPane0.add(Number0);
	contentPane1.add(Number1);
	contentPane2.add(Number2);
	contentPane3.add(Number3);
	contentPane4.add(Number4);
	contentPane5.add(Number5);
	contentPane6.add(Number6);
	contentPane7.add(Number7);
	contentPane8.add(Number8);
	contentPane9.add(Number9);
	contentPane10.add(Number10);
	contentPane11.add(Number11);
	contentPane12.add(Number12);
	contentPane13.add(Number13);
	contentPane14.add(Number14);
	contentPane15.add(Number15);
	contentPane16.add(Number16);
	contentPane.add(contentPane0);
	contentPane.add(contentPane1);
	contentPane.add(contentPane2);
	contentPane.add(contentPane3);
	contentPane.add(contentPane4);
	contentPane.add(contentPane5);
	contentPane.add(contentPane6);
	contentPane.add(contentPane7);
	contentPane.add(contentPane8);
	contentPane.add(contentPane9);
	contentPane.add(contentPane10);
	contentPane.add(contentPane11);
	contentPane.add(contentPane12);
	contentPane.add(contentPane13);
	contentPane.add(contentPane14);
	contentPane.add(contentPane15);
	contentPane.add(contentPane16);
	}

	public JPanel getTimeScale0() {
		return contentPane0;
	}
	public JPanel getTimeScale1() {
		return contentPane1;
	}
	public JPanel getTimeScale2() {
		return contentPane2;
	}
	public JPanel getTimeScale3() {
		return contentPane3;
	}
	public JPanel getTimeScale4() {
		return contentPane4;
	}
	public JPanel getTimeScale5() {
		return contentPane5;
	}
	public JPanel getTimeScale6() {
		return contentPane6;
	}
	public JPanel getTimeScale7() {
		return contentPane7;
	}
	public JPanel getTimeScale8() {
		return contentPane8;
	}
	public JPanel getTimeScale9() {
		return contentPane9;
	}	
public JPanel getTimeScale10() {
		return contentPane10;
	}	
public JPanel getTimeScale11() {
		return contentPane11;
	}	
public JPanel getTimeScale12() {
		return contentPane12;
	}	
public JPanel getTimeScale13() {
		return contentPane13;
	}	
public JPanel getTimeScale14() {
		return contentPane14;
	}	
public JPanel getTimeScale15() {
		return contentPane15;
	}	
public JPanel getTimeScale16() {
		return contentPane16;
	}	
	
}
