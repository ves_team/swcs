package logic;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class GUI_ProgressBar {
	private JFrame frmCreationOfCards;
	private JLabel lblDescription;
	private JLabel lblProgress;
	private JLabel lblProgressCount;

			
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI_ProgressBar window = new GUI_ProgressBar();
					window.frmCreationOfCards.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI_ProgressBar() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCreationOfCards = new JFrame("Cr�ation des cartes");
		frmCreationOfCards.setTitle("SWCS Sequence creation.");
		frmCreationOfCards.setBounds(100, 100, 449, 182);
		frmCreationOfCards.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCreationOfCards.getContentPane().setLayout(null);
		frmCreationOfCards.setVisible(true);
		
		JLabel lblTitle = new JLabel("Sequence creation in progress...");
		lblTitle.setBounds(10, 11, 273, 20); // x, y, width, height
		frmCreationOfCards.getContentPane().add(lblTitle);
		
		lblProgress = new JLabel();
		lblProgress.setText("Status:");
		lblProgress.setBounds(10, 31, 56, 20);
		frmCreationOfCards.getContentPane().add(lblProgress);
		
		lblProgressCount = new JLabel();
		lblProgressCount.setText("");
		lblProgressCount.setBounds(58, 31, 361, 48);
		frmCreationOfCards.getContentPane().add(lblProgressCount);
		
		lblDescription = new JLabel();
		lblDescription.setText("Please wait");
		lblDescription.setBounds(10, 81, 100, 20);
		frmCreationOfCards.getContentPane().add(lblDescription);
	}
	
	
	/** 					Getters & Setters
	***************************************************************/
	public String getProgressCount() {
		return lblProgressCount.getText();
	}

	public void setProgressCount(String sCount) {
		lblProgressCount.setText(sCount);
	}

	public void setFrameVisibility(boolean visible) {
		frmCreationOfCards.setVisible(visible);
	}
}
