package logic;

public class SystemSettings {

	// field variables
	private static String osArchitecture = "unknown";
	private static String osName = "unknown";
	
	public String getOSInformations() {		
		
		// ---------------------> ARCHITECTUR <---------------------
	    // for the 32-bit folder on 64-bit PC's
	    if (System.getProperty("os.arch").equals("x86")) {
	    	osArchitecture = "x86";
	    } else {
	    	osArchitecture = "x64";
	    }
	    
		// ---------------------> OPERATING SYSTEM <---------------------
		// User Manifest File (PATH) - in different Operating Systems
	    if  (System.getProperty("os.version").equals("6.2")) {
	    	osName = "Windows 8";
		} else if  (System.getProperty("os.version").equals("6.1")) {
			osName = "Windows 7";
		} else if  (System.getProperty("os.name").equals("Windows Vista")) {
			osName = "Windows Vista"; 
		} else if  (System.getProperty("os.name").equals("Windows XP")) {
			osName = "Windows XP";
		}
	    
	    return "using: ("+osName+") with ("+osArchitecture+")";
	}
	
	
	
	 /** 						Getters & Setters
	 ***************************************************************/
	public String getOSArchitecture() {
		return osArchitecture;
	} 
	public String getOSName() {
		return osName;
	}
}
