package logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.util.SystemOutLogger;
import org.apache.poi.xssf.usermodel.XSSFSheet; // library: poi-ooxml-3.10-FINAL-20140208.jar
import org.apache.poi.xssf.usermodel.XSSFWorkbook; // library: poi-ooxml-3.10-FINAL-20140208.jar
// library: poi-3.10-FINAL-20140208.jar
// library: poi-3.10-FINAL-20140208.jar

public class Excel {
	
	// -------------
	// choose excel file
	public File chooseFile(final String startFolder) {

		// variables
		File choosedFile = null;
		
		// JFileChooser dialog box
		JFileChooser fc = new JFileChooser(startFolder);
	    FileNameExtensionFilter filter = new FileNameExtensionFilter("Excel Files", "xlsx", "xlsm"); // show only excel files to choose
	    fc.setFileFilter(filter);
	    int rueckgabeWert = fc.showOpenDialog(null); // open dialog box and look for return statement
    
	    // if user choosed a valid file? 
	    if(rueckgabeWert == JFileChooser.APPROVE_OPTION) {
	    	File file = fc.getSelectedFile();
	        choosedFile = file.getAbsoluteFile();
			System.out.println("  choose excel file: "+file.getName());
	    } else {
	    	System.out.println("  Exit: no excel file choosed");
	    	System.exit(0);
	    }
	    
	    return choosedFile;
	}

	
	
	// -------------	
	// initialize excel file
	public XSSFSheet initializeExcel(File excelFile, int iSheet) {
		
		try {
			FileInputStream file = new FileInputStream(excelFile);
		    XSSFWorkbook workbook = new XSSFWorkbook (file); // get the workbook instance for XLS file 
		    return workbook.getSheetAt(iSheet); // get first sheet from the workbook
		    
		} catch (IOException e) {
		    e.printStackTrace();
		}
		return null;
	}
	
	
	
	// -------------	
	// read information from excel file (*.xlsx)
	public String[][] readXLSX(XSSFSheet sheet, int iRowFirst, int iRowLast, int iColumnFirst, int iColumnLast) {
		ConfigFile conf=new ConfigFile();
		String arrExcel[][] = null;
		int iLastRowInExcel;
		
	    // check for input variables
	    if (iRowLast <= 0) {
	    	int last=0;
	    	for (int i = Integer.parseInt(conf.getRowFirst()); i <sheet.getPhysicalNumberOfRows(); i++) {
	    		if(sheet.getRow(i).getCell(conf.getDescripitionCell()).getStringCellValue().equals("") /*&& sheet.getRow(i).getCell(conf.getSequenceCell()).getNumericCellValue()==0.0*/){
					last=i;
					break;
				}
			}
	    	iLastRowInExcel = sheet.getPhysicalNumberOfRows(); // no value set by user -> take last written row
	    	if(last>Integer.parseInt(conf.getRowFirst())) iLastRowInExcel=last;
	    	System.out.println("phy:"+sheet.getPhysicalNumberOfRows()+" -> "+last);
	    } else {
	    	iLastRowInExcel = iRowLast;
	    }

	    // define array size
	    arrExcel = new String[(iLastRowInExcel - iRowFirst + 1)][iColumnLast]; 

		for (int i = iRowFirst; i <= iLastRowInExcel; i++) {
			for (int j = 0; j < iColumnLast; j++) {
				Cell cell = sheet.getRow(i - 1).getCell(j); // getRow starts by 0
				try {
					switch(cell.getCellType()) {
	                case Cell.CELL_TYPE_NUMERIC:
	                	arrExcel[i - iRowFirst][j] = cellNumeric(cell);
	                    break;
	        			
	                case Cell.CELL_TYPE_STRING:
	                	arrExcel[i - iRowFirst][j] = cell.getStringCellValue();
	                    break;
	                
	                case Cell.CELL_TYPE_FORMULA:
	                	switch(cell.getCachedFormulaResultType()) {
	                	case Cell.CELL_TYPE_NUMERIC:
	                		arrExcel[i - iRowFirst][j] = cellNumeric(cell);
		                	break;
	                	case Cell.CELL_TYPE_STRING:
	                		arrExcel[i - iRowFirst][j] = cell.getStringCellValue();
	                		break;
	                	}
					}
				} catch (NullPointerException e) {
					arrExcel[i - iRowFirst][j] = ""; // cell is blank
				}
			} // check next column
		} // check next row
		
		System.out.println("  read data records from excel file");
		return arrExcel;
	}
	
	
	// =============	
	// if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC)
	private String cellNumeric(Cell cell) {
		
		if (DateUtil.isCellDateFormatted(cell)) {
    		String sDate = new SimpleDateFormat("dd.MM.yyyy").format(cell.getDateCellValue());
    		return sDate;
    	} else {
        	double dValue = cell.getNumericCellValue();
        	if (dValue > 0) {
            	String sValue = String.valueOf(dValue);
            	if (sValue.endsWith(".0")) {
            		return sValue.substring(0, sValue.length() - 2);
            	} else {
            		return sValue;
            	}
        	}
    	}
		return null;
	}
}
