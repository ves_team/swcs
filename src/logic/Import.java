package logic;

import java.io.File;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class Import {
	
	Manifest manifest = new Manifest();
	Kanban kanban = new Kanban();

	// -------------
	// create kanban .galleryitem
	public void importKanbansFromNewItems() throws XPathExpressionException, TransformerException, ParserConfigurationException {
		
		// set all galleryitems in folder 'new items' to array list
		File[] arrNewItems = kanban.getNewItems().listFiles();

		// set all existing galleryitems in folder 'inbox' to array list
		// File[] arrInbox = kanban.getToInbox().listFiles();

		
		if (arrNewItems != null) {

			// add kanbans from 'new items'
			for (int i = 0; i < arrNewItems.length; i++) {

				// cut file suffix (.galleryitem)
				String sFilename = arrNewItems[i].getName().replaceFirst("[.][^.]+$", ""); // gallery name

				// MOVE this file to INBOX folder
				File sourceFile = arrNewItems[i];
				File destPath = kanban.getToInbox();
				sFilename = moveFile(sourceFile, destPath);
				
				// INSERT XML CODE
				insertXMLContent(sFilename);
			}
			System.out.println("  kanbans imported into gallery");
		}
	}
	
	// =============
	// insert new kanban in user manifest
	public void insertXMLContent(String sFilename) throws TransformerException, ParserConfigurationException, XPathExpressionException {
		
		// ADAPT USER MANIFEST FILE (ITEM)
		Document docUserManifest = manifest.getDoc();
		String rawXPathOrganization = String.format("//organization[1]/item/item/item");
		XPathExpression exprOrganization = manifest.getXPath().compile(rawXPathOrganization);
		Object resultOrganization = exprOrganization.evaluate(docUserManifest, XPathConstants.NODESET);
		NodeList nodesOrganization = (NodeList) resultOrganization;
		
		// item
		Element itemElement = docUserManifest.createElement("item");
		itemElement.setAttribute("identifier", "id-" + sFilename);
		itemElement.setAttribute("identifierref", "id-" + sFilename + "_ref");
		nodesOrganization.item(0).appendChild(itemElement);
		
		// metadata
		Element metadataElement = docUserManifest.createElement("metadata");
		itemElement.appendChild(metadataElement);
		
		// lom:general
		Element generalElement = docUserManifest.createElement("lom:general");
		metadataElement.appendChild(generalElement);
		
		// lom:title
		Element titleElement = docUserManifest.createElement("lom:title");
		generalElement.appendChild(titleElement);
		
		// lom:string
		Element stringTitleElement = docUserManifest.createElement("lom:string");
		stringTitleElement.setAttribute("language", "en");
		stringTitleElement.setTextContent(sFilename + " ");
		titleElement.appendChild(stringTitleElement);
		
		// lom:keyword
		Element keywordElement = docUserManifest.createElement("lom:keyword");
		generalElement.appendChild(keywordElement);
		
		// lom:string
		Element stringKeywordElement = docUserManifest.createElement("lom:string");
		stringKeywordElement.setAttribute("language", "en");
		stringKeywordElement.setTextContent("(c) Valessentia Solutions");
		keywordElement.appendChild(stringKeywordElement);
		
		// lom:technical
		Element technicalElement = docUserManifest.createElement("lom:technical");
		metadataElement.appendChild(technicalElement);
		
		// lom:format
		Element formatElement = docUserManifest.createElement("lom:format");
		formatElement.setTextContent("application/x-smarttech-galleryitem;x-original-type=image/xml+svg");
		technicalElement.appendChild(formatElement);
		
		
		// ADAPT USER MANIFEST FILE (RESOURCE)
		String rawXPathResource = String.format("//resources[1]");
		XPathExpression exprResource = manifest.getXPath().compile(rawXPathResource);
		Object resultResource = exprResource.evaluate(docUserManifest, XPathConstants.NODESET);
		NodeList nodesResource = (NodeList) resultResource;
		
		// resource
		Element resourceElement = docUserManifest.createElement("resource");
		resourceElement.setAttribute("identifier", "id-" + sFilename + "_ref");
		resourceElement.setAttribute("href", kanban.getToInbox().getName() + "/" + sFilename + ".galleryitem");
		nodesResource.item(0).appendChild(resourceElement);
		
		// file
		Element fileElement = docUserManifest.createElement("file");
		fileElement.setAttribute("href", kanban.getToInbox().getName() + "/" + sFilename + ".galleryitem");
		resourceElement.appendChild(fileElement);
		
		
		// SAVE THE FILE
		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(docUserManifest);
		StreamResult streamResult = new StreamResult(new File(manifest.getUserManifest()));
	 
		transformer.transform(source, streamResult);
	}
		

	
	// =============
	// move files in other directory
	public String moveFile(File sourceFile, File destPath) {
		int i = 0;
		String sFilenameOriginal = sourceFile.getName().replaceFirst(".galleryitem", ""); // sourceFile with path, but without suffix (= .galleryitem)
		
		try {
			String sFilename = sFilenameOriginal; // filename without suffix
			while (!sourceFile.renameTo(new File(destPath + "/" + sFilename.concat(".galleryitem")))) { // name still exist?
				i++;
				sFilename = sFilenameOriginal.concat(" (" + i + ")");
			}
			return sFilename;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sFilenameOriginal;
	}
	
	public String method(String str) {
	    
	    return str;
	}
		
		/** keep exist kanbans in INBOX folder **/ // to develope
//		if (f2 != null) {
//
//			// Add existing Objects from INBOX folder
//			for (int x = f2.listFiles().length - 1; x >= 0; x--) {
//
//				System.out.println("Length::::: "+f2.listFiles().length);
//				// Define Gallery-Item
//				String sFilename = fileArray2[x].getName().replaceFirst("[.][^.]+$", ""); // primary key
//
//				// INSERT XML CODE
//				insertXMLContent(sFilename, nodesItem);
//			}
//		}
}