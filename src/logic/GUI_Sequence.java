package logic;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class GUI_Sequence extends JPanel {

	// constants
	private final static int guiHeight = 30;
	private final static int guiWidth = 530; // if you change this value don't forget to change it in kanban -> image too !
	Color lightBlue = new Color(135, 206, 250);
	Color darkBlue = new Color(100, 170, 215);
	Color lightGrey = new Color(220, 220, 220);
	Color silverGrey = new Color(192, 192, 192);
	Color darkGrey = new Color(169, 169, 169);
	Color lightRed = new Color(255, 210, 210);
	Color lightBrown = new Color(180, 150, 100);
	
	// variables
	private JPanel contentPane,contentPane0,contentPane1,contentPane2,contentPane3,contentPane4;
	private JPanel contentPanePreview;
	private LineBorder lBorder = new LineBorder(new Color(100, 100, 100));
	private EmptyBorder gapBetweenBorderAndText = new EmptyBorder(2, 10, 2, 10); // top, right, bottom, left

	public GUI_Sequence(String arrExcel[], boolean bTotal) {
		
		contentPane = new JPanel();
		contentPane.setOpaque(false); // do not show background
		contentPane.setBounds(0, 0, guiWidth, guiHeight); // x-Pos, y-Pos, width, height
		
		contentPane0 = new JPanel();
		contentPane0.setOpaque(false); // do not show background
		contentPane0.setBounds(0, 0, 45, 30); // x-Pos, y-Pos, width, height
		
		contentPane1 = new JPanel();
		contentPane1.setOpaque(false); // do not show background
		contentPane1.setBounds(60, 0, 300, 30); // x-Pos, y-Pos, width, height
		
		contentPane2 = new JPanel();
		contentPane2.setOpaque(false); // do not show background
		contentPane2.setBounds(362, 0, 55, 30); // x-Pos, y-Pos, width, height
		
		contentPane3 = new JPanel();
		contentPane3.setOpaque(false); // do not show background
		contentPane3.setBounds(419, 0, 55, 30); // x-Pos, y-Pos, width, height
		
		contentPane4 = new JPanel();
		contentPane4.setOpaque(false); // do not show background
		contentPane4.setBounds(476, 0, 55, 30); // x-Pos, y-Pos, width, height
		
		
		// Sequence Number
		JLabel lNumber = new JLabel(arrExcel[0]);
		lNumber.setOpaque(false); // do not show background
		lNumber.setHorizontalAlignment(SwingConstants.RIGHT);
		lNumber.setBounds(0, 0, 45, 30); // x-Pos, y-Pos, width, height
		
		// Sequence Title
		JLabel lTitle = new JLabel(arrExcel[1]);
		lTitle.setOpaque(true); // to show background
        lTitle.setBorder(BorderFactory.createCompoundBorder(lBorder, gapBetweenBorderAndText));
		lTitle.setBounds(0, 0, 300, 30); // x-Pos, y-Pos, width, height
		if (bTotal) {
			lTitle.setBackground(lightBlue);
			System.out.println("  created kanban gui");
		} else {
			lTitle.setBackground(lightBlue);
		}
		
		// Sequence Time (manual)
		JLabel lTimeManual = new JLabel(arrExcel[2]);
		lTimeManual.setHorizontalAlignment(SwingConstants.RIGHT);
		lTimeManual.setBackground(lightGrey);
		lTimeManual.setOpaque(true); // to show background
        lTimeManual.setBorder(BorderFactory.createCompoundBorder(lBorder, gapBetweenBorderAndText));
		lTimeManual.setBounds(0, 0, 55, 30); // x-Pos, y-Pos, width, height

		// Sequence Time (machine)
		JLabel lTimeMachine = new JLabel(arrExcel[3]);
		lTimeMachine.setHorizontalAlignment(SwingConstants.RIGHT);
		lTimeMachine.setBackground(silverGrey);
		lTimeMachine.setOpaque(true); // to show background
        lTimeMachine.setBorder(BorderFactory.createCompoundBorder(lBorder, gapBetweenBorderAndText));
		lTimeMachine.setBounds(0, 0, 55, 30); // x-Pos, y-Pos, width, height

		// Sequence Time (movements)
		JLabel lTimeMove = new JLabel(arrExcel[4]);
		lTimeMove.setHorizontalAlignment(SwingConstants.RIGHT);
		lTimeMove.setBackground(darkGrey);
		lTimeMove.setOpaque(true); // to show background
        lTimeMove.setBorder(BorderFactory.createCompoundBorder(lBorder, gapBetweenBorderAndText));
		lTimeMove.setBounds(0, 0, 55, 30);
		
		contentPane0.setLayout(null);
		contentPane1.setLayout(null);
		contentPane2.setLayout(null);
		contentPane3.setLayout(null);
		contentPane4.setLayout(null);
		contentPane.setLayout(null);
		contentPane0.add(lNumber);
		contentPane1.add(lTitle);
		contentPane2.add(lTimeManual);
		contentPane3.add(lTimeMachine);
		contentPane4.add(lTimeMove);
		contentPane.add(contentPane0);
		contentPane.add(contentPane1);
		contentPane.add(contentPane2);
		contentPane.add(contentPane3);
		contentPane.add(contentPane4);
	}
		
	
	public JPanel snapshotPreviewContentPane(String arrExcel[]) {

		contentPanePreview = new JPanel();
		contentPane.setBounds(0, 0, 120, 30); // x-Pos, y-Pos, width, height

		JLabel lSequence = new JLabel(arrExcel[0]);
        lSequence.setBorder(BorderFactory.createCompoundBorder(lBorder, gapBetweenBorderAndText));
		lSequence.setBounds(30, 0, 120, 30); // x-Pos, y-Pos, width, height
	
		contentPanePreview.setLayout(null);
		contentPanePreview.add(lSequence);
		return contentPanePreview;
	}
	
	
	
	/** 						Getters & Setters
	***************************************************************/
	public JPanel getPreview() {
		return contentPane;
	}
	
	public JPanel getNumber() {
		return contentPane0;
	}
	
	public JPanel getTitle() {
		return contentPane1;
	}
	
	public JPanel getManualTime() {
		return contentPane2;
	}
	
	public JPanel getMachineTime() {
		return contentPane3;
	}
	
	public JPanel getMoveTime() {
		return contentPane4;
	}

	public int getGuiHeight() {
		return guiHeight;
	}

	public int getGuiWidth() {
		return guiWidth;
	}
}