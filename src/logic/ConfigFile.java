package logic;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigFile {

    private String projectName;
    private String titles;
    private String subtitles;
	private String standardPathForFileChooser;
	private String rowFirst;
	private String rowLast;
	private String columnFirst;
	private String columnLast;
	private String sheetNr;
	private String templatePath;
	private String s_walkTimeBox;
	private Boolean walkTimeBox;
	
	private Integer moveCell,machineCell,manualCell,descrCell,seqCell;
    
    
	
    
    /**
     * Loads configuration parameters from a textfile and print them out.
     */
	public ConfigFile() {
 
        // load configuration file
    	Properties prop = new Properties();
    	InputStream input = null;
    	String filename = "config.properties";
    	
    	try {
    		// assume properties file �config.properties� is in your project classpath root folder.
    		input = ConfigFile.class.getClassLoader().getResourceAsStream(filename);
    		if (input == null){

    			// config file not loaded
    			System.out.println("  load standard parameters - no config file found");
    		}
    		
    		// load a properties file from class path, inside static method
    		prop.load(input);
 
    		// SMP, gallery titles
    		titles = prop.getProperty("title", "unknow");
    		subtitles = prop.getProperty("subtitle", "unknow");
    		
        	// set parameters from config file to variables (found in config, default)
    		projectName = prop.getProperty("project_name", "UNKNOWN");
    		standardPathForFileChooser = prop.getProperty("path_file_chooser_box", System.getProperty("user.home") + "/Desktop");
        	rowFirst = prop.getProperty("first_row", "1");
        	rowLast = prop.getProperty("last_row", "0"); // if 0 -> takes last written row
        	columnFirst = prop.getProperty("first_column", "1");  // 1 -> column A
        	columnLast = prop.getProperty("last_column", "1");
        	sheetNr = prop.getProperty("sheet1", "0"); // get access to first sheet (0 -> 1st sheet, 1 -> 2nd sheet, ..)
        	templatePath = prop.getProperty("path_template", "UNKNOWN");
        	s_walkTimeBox = prop.getProperty("3rd_column", "false");
        	walkTimeBox = s_walkTimeBox.equals("true");
        	System.out.println(">>> 3rd colums :"+walkTimeBox);
            
        	moveCell = Integer.parseInt(prop.getProperty("move_cell","null"));
        	seqCell = Integer.parseInt(prop.getProperty("sequence_cell","null"));
        	machineCell = Integer.parseInt(prop.getProperty("machine_cell","null"));
        	manualCell = Integer.parseInt(prop.getProperty("manual_cell","null"));
        	descrCell = Integer.parseInt(prop.getProperty("description_cell","null"));
        	
        	// config file loaded
        	System.out.println("  load parameters from config file");
        	
    	} catch (IOException ex) {
    		ex.printStackTrace();
        } finally{
        	if(input!=null) {
        		try {
        			input.close();
        		} catch (IOException e) {
        			e.printStackTrace();
        		}
        	}
        }
    }
	
	
	 /** 						Getters & Setters
	 ***************************************************************/
	public String getProjectName() {
		return projectName;
	}

	public String getTitles() {
		return titles;
	}
	
	public String getSubitles() {
		return subtitles;
	}
	
	public String getStandardPathForFileChooser() {
		 return standardPathForFileChooser;
	}

	public String getRowFirst() {
		return rowFirst;
	}

	public String getRowLast() {
		return rowLast;
	}

	public String getColumnFirst() {
		return columnFirst;
	}

	public String getColumnLast() {
		return columnLast;
	}

	public String getSheet() {
		return sheetNr;
	}
	
	public String getTemplatePath() {
		return templatePath;
	}
	
	public Boolean getWalktimeBox() {
		return walkTimeBox;
	}
	public Integer getMoveCell(){
		return moveCell;
	}
	public Integer getMachineCell(){
		return machineCell;
	}
	public Integer getManualCell(){
		return manualCell;
	}
	public Integer getDescripitionCell(){
		return descrCell;
	}
	public Integer getSequenceCell(){
		return seqCell;
	}
}
