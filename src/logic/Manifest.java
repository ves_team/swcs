package logic;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class Manifest {

	// variables
	SystemSettings systemSettings = new SystemSettings();
	private String systemManifestFile = "unknown";
	private String userManifestPath = "unknown";
	private DocumentBuilderFactory docFactory;
	private DocumentBuilder docBuilder;
	private static Document docUserManifest;
	private static XPath xPath;
	
	
	
	// -------------
	// check for exist user manifest file in addedcontent folder
	public boolean checkExistUserManifest() {
	    
		if (new File(getUserManifest()).exists()) {
			return true;
	    }
		else return false;
	}
	

	// -------------
	// create a new user manifest file
	public void newUserManifest(String projectName, String galleryTitle, String gallerySubTitle) {
		
		try {
			// CREATE NEW FILE
			docFactory = DocumentBuilderFactory.newInstance();
			docBuilder = docFactory.newDocumentBuilder();
			docUserManifest = docBuilder.newDocument();
	 
			
			// FILL THE FILE
			// manifest (1st line)
			Element manifestElement = docUserManifest.createElement("manifest");
			manifestElement.setAttribute("xmlns:lom", "?");
			docUserManifest.appendChild(manifestElement);
						
			// metadata entry block (metadata > general > identifier > entry+attr)
			Node metadataEntryBlock01 = newMetadataEntryBlock("http://tempuri.org/default?manifest=Custom%20Content"); 
			manifestElement.appendChild(metadataEntryBlock01);
			
			// organizations
			Element organizationsElement = docUserManifest.createElement("organizations");
			manifestElement.appendChild(organizationsElement);
			
			// organization
			Element organizationElement = docUserManifest.createElement("organization");
			organizationsElement.appendChild(organizationElement);
			
			// metadata entry block (metadata > general > identifier > entry+attr)
			Node metadataEntryBlock02 = newMetadataEntryBlock("http://tempuri.org/default?organization=Custom%20Content");
			organizationElement.appendChild(metadataEntryBlock02);
			
			// item
			Element itemElement = docUserManifest.createElement("item");
			itemElement.setAttribute("projectname", projectName); // project title
			organizationElement.appendChild(itemElement);
			
			// metadata entry block (metadata > general > identifier > entry+attr)
			Node medataEntryBlock03 = newMetadataEntryBlock("http://tempuri.org/Custom%20Content"); 
			itemElement.appendChild(medataEntryBlock03);
			
			/** ---- move to own method ---- **/
			// title block (item+attr > metadata > title > string+attr)
			Node titleBlock01 = newTitleBlock(galleryTitle);
			itemElement.appendChild(titleBlock01);
			Node titleBlock02 = newTitleBlock(gallerySubTitle);
			titleBlock01.appendChild(titleBlock02);
			
			// resources
			Element resourcesElement = docUserManifest.createElement("resources");
			manifestElement.appendChild(resourcesElement);
						
			// SAVE THE FILE
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(docUserManifest);
			StreamResult result = new StreamResult(new File(getUserManifest()));
	 
			transformer.transform(source, result);

			// ERROR HANDLING
		  } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  }
	}
		
	// =============
	// create a metadata entry block (metadata > general > identifier > entry+attr) 
	public Node newMetadataEntryBlock(String entryString) {
		
		// metadata
		Element metadataElement = docUserManifest.createElement("metadata");
		
		// lom:general
		Element generalElement = docUserManifest.createElement("lom:general");
		metadataElement.appendChild(generalElement);
		
		// lom:identifier
		Element identifierElement = docUserManifest.createElement("lom:identifier");
		generalElement.appendChild(identifierElement);
	
		// lom:entry
		Element entryElement = docUserManifest.createElement("lom:entry");
		entryElement.appendChild(docUserManifest.createTextNode(entryString));
		identifierElement.appendChild(entryElement);
		
		// give nodeList back
		return metadataElement;
	}
	
	// =============
	// create a title block (item+attr > metadata > title > string+attr)
	public Node newTitleBlock(String title) {
		
		// item
		Element itemElement = docUserManifest.createElement("item");
		itemElement.setAttribute("identifier", "id-" + title);
		
		// meta
		Element metadataElement = docUserManifest.createElement("metadata");
		itemElement.appendChild(metadataElement);
		
		// lom:general
		Element generalElement = docUserManifest.createElement("lom:general");
		metadataElement.appendChild(generalElement);
		
		// lom:title
		Element titleElement = docUserManifest.createElement("lom:title");
		generalElement.appendChild(titleElement);
	
		// lom:entry element
		Element stringElement = docUserManifest.createElement("lom:string");
		stringElement.appendChild(docUserManifest.createTextNode(title));
		titleElement.appendChild(stringElement);
		
		// give nodeList back
		return itemElement;
	}


	// -------------
	// parse user manifest
	public void parseUserManifest() throws SAXException, IOException, ParserConfigurationException, XPathExpressionException {
		
		// CREATE NEW FILE
		docFactory = DocumentBuilderFactory.newInstance();
		docBuilder = docFactory.newDocumentBuilder();
		docUserManifest = docBuilder.newDocument();
		
		// User Manifest File
		docUserManifest = docBuilder.parse(getUserManifest());
		xPath = XPathFactory.newInstance().newXPath();
	}
	
	
	// -------------
	// check for manifest of the same project
	public Node getProjectNameInManifest() throws XPathExpressionException {
		
		String rawXPath = String.format("//organization[1]/item");
		XPathExpression expr = xPath.compile(rawXPath);
		Object result = expr.evaluate(docUserManifest, XPathConstants.NODESET);
		NodeList nodes = (NodeList) result;

		return nodes.item(0).getAttributes().item(0);
	}
	
	
	
	
	/** 						Getters & Setters
	***************************************************************/
	public XPath getXPath() {
		return xPath;
	}
	
	
	public Document getDoc() {
		return docUserManifest;
	}
	
	
	public String getSystemManifest() { // give back (system Manifest fullname)
		switch (systemSettings.getOSArchitecture()) {
		case "x86":
			systemManifestFile = System.getenv("ProgramFiles")+"/Common Files/SMART Technologies/Gallery/imsmanifest.xml";
			break;
		case "x64":
			systemManifestFile = System.getenv("ProgramFiles")+" (x86)/Common Files/SMART Technologies/Gallery/imsmanifest.xml"; //located in 32-bit folder
			break;
		}
		return systemManifestFile;
	}
	
	
	public String getUserManifest() { // give back (user Manifest fullname)
//		return "C:/Dokumente und Einstellungen/All Users/Dokumente/SMART Technologies/Gallery/Added Content/imsmanifest.xml";
		return "C:/USERS/Public/Documents/SMART Technologies/Gallery/Added Content/imsmanifest.xml";
	}
	
	
	public String getUserManifestPath() { // give back (user Manifest path)
		//switch (systemSettings.getOSName()) {
		//case "Windows 8": case "Windows 7": case "Windows Vista": case "Windows XP": 
			userManifestPath = "C:/USERS/Public/Documents/SMART Technologies/Gallery/Added Content";
			//break;
		//}
		return userManifestPath;
	}
}
