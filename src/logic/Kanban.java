package logic;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Kanban {

	Manifest manifest = new Manifest();
	private File toKanban;
	private static File newItems;
	private static File toInbox;
	List<String> fileList;
	private Double timeUnitiy=0.0;
	
	public Double checkCell(String cell, int i, String rank){
		Double doubleCell=0.0;
		try {
			 doubleCell = Double.parseDouble(cell) / timeUnitiy * 24;
		 }catch (NumberFormatException e){
			 if (cell.equals(" ")){
				 final JPanel panel = new JPanel();
				 JOptionPane.showMessageDialog(panel, "this cell : ["+(i+3)+";"+rank+"] is not empty", "XLS WARNING",
					        JOptionPane.INFORMATION_MESSAGE);
			 } else{
				 final JPanel panel = new JPanel();
					JOptionPane.showMessageDialog(panel, "incompatible format in cell : ["+(i+3)+";"+rank+"] => \""+cell+"\" is not a number", "XLS ERROR",JOptionPane.INFORMATION_MESSAGE);
			 }
			 doubleCell=0.0;
		 }
		
		return doubleCell;
	}
	
	// -------------
	// create new folder in added content folder
	/**
	 * @wbp.parser.entryPoint
	 */
	public void createSubFolder() {
		
		try {
	        // create Sub-Folders
			toKanban = new File(manifest.getUserManifestPath()+"/to kanban"); // folder goes to .galleryitem
			newItems = new File(manifest.getUserManifestPath()+"/new items"); // folder has the temp kanbans during creating process
			toInbox = new File(manifest.getUserManifestPath()+"/inbox"); // folder goes to gallery inbox
	    	File kanbanImageFolder = new File(toKanban.getPath()+"/images");
	    
	    	toKanban.mkdirs();
	    	newItems.mkdirs();
	    	toInbox.mkdirs();
	    	kanbanImageFolder.mkdirs();
	    	
	    	System.out.println("  created subfolders");
	    	
	    } catch (Exception e) {
	        e.getMessage();
	    }	
	}
	
	
	// -------------
	// create kanban metadata 
	public void createMetadata(String iconName) {
		
		try {
			// CREATE NEW FILE
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			
			
			// FILL THE FILE
			// manifest element (1st line)
			Element lomElement = doc.createElement("lom:lom");
			lomElement.setAttribute("xmlns:lom", "http://ltsc.ieee.org/xsd/LOM");
			doc.appendChild(lomElement);
		
			// lom:general
			Element lomGeneral = doc.createElement("lom:general");
			lomElement.appendChild(lomGeneral);
		
			// lom:title
			Element lomTitle = doc.createElement("lom:title");
			lomGeneral.appendChild(lomTitle);
			
			// lom:string
			Element lomTitleString = doc.createElement("lom:string");
			lomTitleString.setAttribute("language", "en");
			lomTitleString.setTextContent(iconName);
			lomTitle.appendChild(lomTitleString);
			
			// lom:keyword
			Element lomKeyword = doc.createElement("lom:keyword");
			lomGeneral.appendChild(lomKeyword);
			
			// lom:string
			Element lomKeywordString = doc.createElement("lom:string");
			lomKeywordString.setAttribute("language", "en");
			lomKeywordString.setTextContent("(c) Valessentia Solutions");
			lomKeyword.appendChild(lomKeywordString);
			
			
			// SAVE THE FILE
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(toKanban.getPath()+"/metadata.xml"));
		 
			transformer.transform(source, result);
			System.out.println("  created kanban metadata");
		
		// ERROR HANDLING
		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
	}
	
	
	// -------------
	// create kanban svg
	/**
	 * @wbp.parser.entryPoint
	 */
	public void createSVG(String arrExcel[][]) throws ParserConfigurationException, IOException, TransformerException {

		System.out.println("svg :");
		new File(getToKanban().getPath()+"/page0.svg").delete();
		ConfigFile config = new ConfigFile(); // load parameters for Excel
		
		// variables
		Double gapPrefixElement = 30.0; // going to left (minus)
		Double gapSuffixElement = 2.0;
		Double yPosition = 0.0;
		
		// CREATE NEW FILE
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.newDocument();
			
		// svg
		Element svgElement = doc.createElement("svg");
		doc.appendChild(svgElement);

		// group
		Element GroupElement = doc.createElement("g");
		GroupElement.setAttribute("class", "group");
		svgElement.appendChild(GroupElement); //*/
		String s_timeUnitiy;
		String message_unity="";
		String message_takt="";
		
		Double taktTime=0.0;
		Boolean bool_unity=false;
		Boolean bool_takt=false;
		
		do{
			s_timeUnitiy  = JOptionPane.showInputDialog(null, "unity of time ?"+message_unity, "time unity", JOptionPane.QUESTION_MESSAGE);
			try {
		    	 timeUnitiy = Double.parseDouble(s_timeUnitiy);
			} catch (NumberFormatException e) {
				e.getMessage();
				bool_unity=true;
				message_unity = "  put a numeric value. ";
			}
			if(timeUnitiy==0)
				message_unity=message_unity+"Not null !";
			if(timeUnitiy!=0)
				bool_unity=false;
		}while(bool_unity);
	    System.out.println(">>>> time unity : "+timeUnitiy);
	    
	 // takt time init
	    do{
	    	String s_taktTime = JOptionPane.showInputDialog(null, "takt time ?"+message_takt, "takt time", JOptionPane.QUESTION_MESSAGE);
			try {
		    	 taktTime = Double.parseDouble(s_taktTime);
			} catch (NumberFormatException e) {
				bool_takt=true;
				message_takt = "  put a numeric value. ";
			}
			if(taktTime==0)
				message_takt=message_takt+"Not null !";
			if(taktTime!=0)
				bool_takt=false;
		}while(bool_takt);
	    System.out.println(">>>> takt time : "+taktTime);
	    
		
		for (int i = 0; i < arrExcel.length; ++i) {
			// group
			Element childGroupElement = doc.createElement("g");
			/*child*/GroupElement.setAttribute("class", "group");
			GroupElement.appendChild(childGroupElement);	
			Double actualRectSize_1 = (double)0;
			Double actualRectSize_2 = (double)0;
			Double actualRectSize_3 = (double)0;
		    
				if (arrExcel[i][2] != null || arrExcel[i][3] != null || arrExcel[i][1] != null) {
/*
					if(config.getWalktimeBox()){
						// arrow moving time.
						if (arrExcel[i][4] != null){
							try{
							actualRectSize_3 = Double.parseDouble(arrExcel[i][4]);
							// arrow prefix element 
							Element arrow = doc.createElement("line");
							arrow.setAttribute("x1", String.valueOf(65 - 32 - gapPrefixElement - actualRectSize_3));
							arrow.setAttribute("y1", String.valueOf(yPosition+16));
							arrow.setAttribute("x2", String.valueOf(65 - 32 - gapPrefixElement));
							arrow.setAttribute("y2", String.valueOf(yPosition+16));
							arrow.setAttribute("stroke-linecap", "round");
							arrow.setAttribute("stroke", "#000000");
							arrow.setAttribute("stroke-width", "3");
							arrow.setAttribute("marker-end", "url(#ArrowEnd0)");
							GroupElement.appendChild(arrow);
							} catch (Exception e){}
						}
					}
					
					if (arrExcel[i][3] != null){
						try{
						 actualRectSize_2=Double.parseDouble(arrExcel[i][3]);
						// time bar (prefix Element)
						Element rectPrefix_2 = doc.createElement("rect");
						rectPrefix_2.setAttribute("height", "30");
						rectPrefix_2.setAttribute("width", String.valueOf(actualRectSize_2));
						rectPrefix_2.setAttribute("x", String.valueOf(65 - 32 - gapPrefixElement - actualRectSize_2 - actualRectSize_3));
						rectPrefix_2.setAttribute("y", String.valueOf(yPosition));
						rectPrefix_2.setAttribute("fill", "#1A1AFF");
						rectPrefix_2.setAttribute("stroke", "#000000");
						rectPrefix_2.setAttribute("stroke-width", "1");
						GroupElement.appendChild(rectPrefix_2);
					} catch (Exception e){}
					}
					
					if (arrExcel[i][2] != null) {
						try {
						actualRectSize_1=Double.parseDouble(arrExcel[i][2]);
						// time bar (prefix Element)
						Element rectPrefix_1 = doc.createElement("rect");
						rectPrefix_1.setAttribute("height", "30");
						rectPrefix_1.setAttribute("width", String.valueOf(actualRectSize_1));
						rectPrefix_1.setAttribute("x", String.valueOf(65 - 32 - gapPrefixElement  - actualRectSize_3 - actualRectSize_2 - actualRectSize_1)); // - 32 to create a gap between 1st rect and img 
						rectPrefix_1.setAttribute("y", String.valueOf(yPosition));
						rectPrefix_1.setAttribute("fill", "#FFFFE2");
						rectPrefix_1.setAttribute("stroke", "#000000");
						rectPrefix_1.setAttribute("stroke-width", "1");
						GroupElement.appendChild(rectPrefix_1);
						} catch (Exception e){}
					}*/
						
					if (arrExcel[i][2] != null) {
						try {
							 actualRectSize_1 = Double.parseDouble(arrExcel[i][2]) / timeUnitiy * 24;
							// time bar (suffix Element)
								Element rectSuffix_1 = doc.createElement("rect");
								rectSuffix_1.setAttribute("height", String.valueOf(30));
								rectSuffix_1.setAttribute("width", String.valueOf(actualRectSize_1));
								rectSuffix_1.setAttribute("x", String.valueOf(535 + gapSuffixElement));
								rectSuffix_1.setAttribute("y", String.valueOf(yPosition));
								rectSuffix_1.setAttribute("fill", "#FFFFC8");
								rectSuffix_1.setAttribute("stroke", "#000000");
								rectSuffix_1.setAttribute("stroke-width", "1");
								/*child*/GroupElement.appendChild(rectSuffix_1);
						 }catch (NumberFormatException e){
							 actualRectSize_1=0.0;
						 }
						
					} 
						
							
					if (arrExcel[i][3] != null){
						try {
							 actualRectSize_2 = Double.parseDouble(arrExcel[i][3]) / timeUnitiy * 24;
							 Element rectSuffix_2 = doc.createElement("rect");
							 rectSuffix_2.setAttribute("height", String.valueOf(30));
							 rectSuffix_2.setAttribute("width", String.valueOf(actualRectSize_2));
							 rectSuffix_2.setAttribute("x", String.valueOf(535 + gapSuffixElement + actualRectSize_1));
						   	 rectSuffix_2.setAttribute("y", String.valueOf(yPosition));
							 rectSuffix_2.setAttribute("fill", "#0000FF");
							 rectSuffix_2.setAttribute("stroke", "#000000");
							 rectSuffix_2.setAttribute("stroke-width", "1");
							 /*child*/GroupElement.appendChild(rectSuffix_2);
						 }catch (NumberFormatException e){
							 actualRectSize_2=0.0;
						 }
						// time bar (suffix Element)
						
					}
					if(config.getWalktimeBox()){
						// arrow
						if (arrExcel[i][4] != null){
							try {
								 actualRectSize_3 = Double.parseDouble(arrExcel[i][4]) / timeUnitiy * 24;
							 }catch (NumberFormatException e){
								 actualRectSize_3=0.0;
							 }
							//code suffix arrow
							Element arrow = doc.createElement("line");
							arrow.setAttribute("x1", String.valueOf(535 /*+ gapPrefixElement */ + actualRectSize_1 + actualRectSize_2));
							arrow.setAttribute("y1", String.valueOf(yPosition+16));
							arrow.setAttribute("x2", String.valueOf(535 /*+ gapPrefixElement */ + actualRectSize_1 + actualRectSize_2 + actualRectSize_3));
							arrow.setAttribute("y2", String.valueOf(yPosition+16));
							arrow.setAttribute("fill", "#0000FF");
							arrow.setAttribute("stroke", "#000000");
							arrow.setAttribute("stroke-width", "3");
							arrow.setAttribute("marker-end", "url(#ArrowEnd0)");
							/*child*/GroupElement.appendChild(arrow);
						}
					}
				}
				
				// image (kanban)
				childGroupElement.setAttribute("class", "group");
				Element frame0 = doc.createElement("image");
				frame0.setAttribute("height", "30");
				frame0.setAttribute("width", "30"); 
				frame0.setAttribute("x", "5");
				frame0.setAttribute("y", String.valueOf(yPosition));
				frame0.setAttribute("xml:id","sequence."+String.valueOf(arrExcel[i][1])+"."+arrExcel[i][5]+".image0");
				frame0.setAttribute("xlink:href", "images/kanban_0_" + i + ".png");
				frame0.setAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");
				childGroupElement.appendChild(frame0);
				
				Element frame1 = doc.createElement("image");
				frame1.setAttribute("height", "30");
				frame1.setAttribute("width", "300"); 
				frame1.setAttribute("x", "60");
				frame1.setAttribute("y", String.valueOf(yPosition));
				frame1.setAttribute("xml:id","sequence."+String.valueOf(arrExcel[i][1])+"."+arrExcel[i][5]+".image1");
				frame1.setAttribute("xlink:href", "images/kanban_1_" + i + ".png");
				frame1.setAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");
				childGroupElement.appendChild(frame1);
				
				Element frame2 = doc.createElement("image");
				frame2.setAttribute("height", "30");
				frame2.setAttribute("width", "55"); 
				frame2.setAttribute("x", "362");
				frame2.setAttribute("y", String.valueOf(yPosition));
				frame2.setAttribute("xml:id","sequence."+String.valueOf(arrExcel[i][1])+"."+arrExcel[i][5]+".image2");
				frame2.setAttribute("xlink:href", "images/kanban_2_" + i + ".png");
				frame2.setAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");
				childGroupElement.appendChild(frame2);
				
				Element frame3 = doc.createElement("image");
				frame3.setAttribute("height", "30");
				frame3.setAttribute("width", "55"); 
				frame3.setAttribute("x", "419");
				frame3.setAttribute("y", String.valueOf(yPosition));
				frame3.setAttribute("xml:id","sequence."+String.valueOf(arrExcel[i][1])+"."+arrExcel[i][5]+".image3");
				frame3.setAttribute("xlink:href", "images/kanban_3_" + i + ".png");
				frame3.setAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");
				childGroupElement.appendChild(frame3);
				
				Element frame4 = doc.createElement("image");
				frame4.setAttribute("height", "30");
				frame4.setAttribute("width", "55"); 
				frame4.setAttribute("x", "476");
				frame4.setAttribute("y", String.valueOf(yPosition));
				frame4.setAttribute("xml:id","sequence."+String.valueOf(arrExcel[i][1])+"."+arrExcel[i][5]+".image4");
				frame4.setAttribute("xlink:href", "images/kanban_4_" + i + ".png");
				frame4.setAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");
				childGroupElement.appendChild(frame4);

				// set position of next kanban (move vertical)
				yPosition += 48;
			
		} 
		
		 //Time scale
		Element childGroupElement = doc.createElement("g");
		childGroupElement.setAttribute("class", "group");
		GroupElement.appendChild(childGroupElement);
		for (int j = 0; j < 161; j++) {
			if (j%10 == 0){
			Element timeScale = doc.createElement("image");
			timeScale.setAttribute("height", "30");
			timeScale.setAttribute("width", "60"); 
			timeScale.setAttribute("x", String.valueOf(511+j*24));
			timeScale.setAttribute("y", "-60");
			timeScale.setAttribute("xml:id",timeUnitiy+".time_unity");
			timeScale.setAttribute("xlink:href", "images/timeScale"+j/10+".png");
			timeScale.setAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");
			childGroupElement.appendChild(timeScale);
			}
			if(j%5 == 0){
				Element line = doc.createElement("line");
				line.setAttribute("x1", String.valueOf(535+(j*24)));
				line.setAttribute("y1", "-16");
				line.setAttribute("x2", String.valueOf(535+(j*24)));
				line.setAttribute("y2", "-30");
				line.setAttribute("stroke-linecap","round");
				line.setAttribute("fill", "#0000FF");
				line.setAttribute("stroke", "#000000");
				line.setAttribute("stroke-width", "4");
				childGroupElement.appendChild(line);
			}
		}
		
	    // takt time
		// group
		Element childGroupElement2 = doc.createElement("g");
		childGroupElement2.setAttribute("class", "group");
		GroupElement.appendChild(childGroupElement2);
	    Element takt = doc.createElement("rect");
		takt.setAttribute("height", String.valueOf(30));
		takt.setAttribute("width", String.valueOf(taktTime/timeUnitiy*24));
		takt.setAttribute("x", String.valueOf(535 + gapSuffixElement));
		takt.setAttribute("y", String.valueOf(yPosition));
		takt.setAttribute("fill", "#FF0000");
		takt.setAttribute("stroke", "#000000");
		takt.setAttribute("stroke-width", "1");
		/*child*/GroupElement.appendChild(takt);
		
		
		
		// SAVE THE FILE
	    
		// write the content into svg file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(getToKanban().getPath()+"/page0.svg"));
		transformer.transform(source, result);
		//readFile(getToKanban().getPath()+"/page0.svg");
		System.out.println("  created kanban svg");
	}
	
	
	// =============
	// create snapshot from JPanel 
	public BufferedImage getSnapShot(JPanel view) {
		int w = view.getWidth();
		int h = view.getHeight();
		BufferedImage image = null;
		boolean excep = true;
		
		while(excep){
			excep = false;
			try{
			image = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
	
			synchronized (view.getTreeLock()) {
				view.paint(image.createGraphics());
			}
	
			}catch (Exception e){
				System.out.println(">> Exception detected");
				excep = true;
			}
		}
		return image;
	}
	
	
	// -------------
	// create kanban .galleryitem
	public void compressZip(String outputZipFile, String sourceFolder) {
		
		// add all files to array list (folder goes recursive through)
		fileList = new ArrayList<String>();
		generateFileList(new File(sourceFolder));
		
		// zip all files in array list 
		byte[] buffer = new byte[1024];
				
		try {
			FileOutputStream fos = new FileOutputStream(outputZipFile);
			ZipOutputStream zos = new ZipOutputStream(fos);

			for (String file : this.fileList) {
				ZipEntry ze = new ZipEntry(file);
				zos.putNextEntry(ze);
				FileInputStream in = new FileInputStream(toKanban.getPath() + File.separator + file);
				
				int len;
				while ((len = in.read(buffer)) > 0) {
					zos.write(buffer, 0, len);
				}
				in.close();
			}

			zos.closeEntry();
			zos.close(); // remember close it
			System.out.println("  created kanban zip (.galleryitem)");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	

	// =============
	// Traverse a directory and get all files, and add the file into fileList
	public void generateFileList(File node) {

		// add file only
		if (node.isFile()) {
			String sFile = node.getAbsoluteFile().toString();
			fileList.add(sFile.substring(toKanban.getPath().length() + 1, sFile.length())); // format the file path for zip
		}

		// go inside directory (recursive)
		if (node.isDirectory()) {
			String[] subNote = node.list();
			for (String filename : subNote) {
				generateFileList(new File(node, filename));
			}
		}
	}


	
	
	/** 						Getters & Setters
	***************************************************************/
	public File getToKanban() {
		return toKanban;
	}
	
	public File getNewItems() {
		return newItems;
	}
	
	public File getToInbox() {
		return toInbox;
	}
	public Double getTimeUnity(){
		return timeUnitiy;
	}
}